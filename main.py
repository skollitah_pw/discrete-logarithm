def find_discrete_log(a, b, z, iterations):
    '''
    b^x(mod z)=a
    :return x
    '''

    temp_power = 1
    count = 0
    result = None

    for i in range(iterations):
        if temp_power % z == a:
            count += 1

            # store first found solution
            if result is None:
                result = i

        temp_power *= b

    print(f'Found solutions: {count}')

    if result is None:
        raise Exception(f'Discrete logarithm not found for a={a}, b={b}, z={z}')

    return result


if __name__ == '__main__':
    try:
        a = int(input('a: '))
        b = int(input('b: '))
        z = int(input('z: '))
        print(f'Discrete logarithm: {find_discrete_log(a, b, z, 10000)}')
    except Exception as e:
        raise e
