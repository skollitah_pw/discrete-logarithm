import unittest

import main


class TestDiscreteLogarithm(unittest.TestCase):

    def test_found_pattern(self):
        self.assertEqual(main.find_discrete_log(8, 5, 13, 50), 3)
        self.assertEqual(main.find_discrete_log(8, 5, 23, 50), 6)
        self.assertEqual(main.find_discrete_log(13, 3, 17, 50), 4)
        pass


if __name__ == '__main__':
    unittest.main()
